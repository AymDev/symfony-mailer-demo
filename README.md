# Symfony Mailer - demo

>Application de démonstration utilisant [Symfony Mailer](https://symfony.com/doc/current/mailer.html) avec [Symfony Messenger](https://symfony.com/doc/current/messenger.html).

## Installation
Cloner le dépôt puis installer les dépendances avec Composer:
```shell
composer install
```

Dupliquer le fichier **.env** en **.env.local** et modifier les variables d'environnement suivantes:

 - `DEFAULT_RECIPIENT`: votre adresse email pour recevoir les emails de test
 - `SENDGRID_KEY`: une clé d'API [SendGrid](https://sendgrid.com/) (pour **Mailer**)
 - `DATABASE_URL`: la DSN pour le SGBD à utiliser (pour **Messenger**)

Créer la base de données:
```shell
./bin/console doctrine:database:create
```
Démarrer le serveur web, et pour *consommer* les messages avec **Messenger**:
```shell
./bin/console messenger:consume async -vv
```
