<?php

namespace App\Controller;

use App\Form\EmailFormType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function homepage(Request $request, MailerInterface $mailer)
    {
        $emailForm = $this->createForm(EmailFormType::class);
        $emailForm->handleRequest($request);

        if ($emailForm->isSubmitted() && $emailForm->isValid()) {
            $email = (new TemplatedEmail())
                ->from(new Address('noreply@example.com'))
                ->to(new Address('john.doe@mail.org', 'John Doe'))
                ->subject('Envoyé avec Symfony Mailer')
                ->htmlTemplate('email.html.twig')
                ->context([
                    'message' => $emailForm['message']->getData(),
                ]);

            $mailer->send($email);
            $this->addFlash('success', 'Message envoyé !');
        }

        return $this->render('home.html.twig', [
            'email_form' => $emailForm->createView()
        ]);
    }
}
