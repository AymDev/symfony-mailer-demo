<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class EmailFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('message', TextareaType::class, [
            'help' => 'Ce message sera envoyé par email.',
            'constraints' => [
                new NotBlank(['message' => 'Veuillez entrer un message.']),
                new Length([
                    'min' => '5',
                    'minMessage' => 'Un peu court, non ? 5 caractères suffiront.',
                ])
            ]
        ]);
    }
}